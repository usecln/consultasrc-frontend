REM compilar el codigo frontend antes de correr este script
REM usar estos dos comandos en cmd para compilar
REM %USERPROFILE%\Documents\consultasrc_fe
REM npm run build

REM Borrar la carpeta static del backend que contiene los archivos compilados
cd %USERPROFILE%\Documents\consultasrc_be\consultasrc
rmdir /S static

cd %USERPROFILE%\Documents\consultasrc_fe\build
del asset-manifest.json

REM renombrar el index para hacerlo vista de laravel (view)
ren index.html index.blade.php

REM copiar archivos staticos compilados
xcopy %USERPROFILE%\Documents\consultasrc_fe\build\static %USERPROFILE%\Documents\consultasrc_be\consultasrc\static /e /i /h

REM copiar el index
del %USERPROFILE%\Documents\consultasrc_be\resources\views\index.blade.php
copy index.blade.php %USERPROFILE%\Documents\consultasrc_be\resources\views\index.blade.php