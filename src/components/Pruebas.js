import React from 'react';
import { connect } from 'react-redux';
import agent from '../agent';
import { Icon, Header, Image } from 'semantic-ui-react';
import {
  PRUEBAS_PAGE_LOADED,
  PRUEBAS_PAGE_UNLOADED,
} from '../constants/actionTypes';

const mapStateToProps = state => ({
  ...state.home,
  appName: state.common.appName,
  token: state.common.token,
  isMobile: state.common.isMobile,
  currentUser: state.common.currentUser,
});

const mapDispatchToProps = dispatch => ({
  onLoad: () =>
    dispatch({ type: PRUEBAS_PAGE_LOADED }),
  onUnload: () =>
    dispatch({  type: PRUEBAS_PAGE_UNLOADED })
});

class Pruebas extends React.Component {
  
  componentWillMount() {
    let p = localStorage.getItem('pruebas');
    if(!p)
      window.location.replace(agent.FOLDER + '/');
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {

    return (
      <div>
        <div style={{ width: '85%', minHeight: '100%' }}>
          <Image src='/closed.jpg' size='huge' style={{ margin: '0 auto'}}/>
          <Header as='h2' style={{ color: '#b38c8c' }}>
            <Icon name='clock' />
            <Header.Content>
              El horario de consultas es de lunes a viernes de 7am a 4pm (UTC-7)
            </Header.Content>
          </Header>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Pruebas);