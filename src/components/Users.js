import React from 'react';
import { SingleDatePicker } from 'react-dates';
import moment from 'moment';
import 'react-dates/lib/css/_datepicker.css';
import 'react-dates/initialize';
import Moment from 'react-moment';
import { connect } from 'react-redux';
import { Button, Form, Table, Modal, Label, Popup, Header, Icon, Checkbox, Dimmer, Loader } from 'semantic-ui-react'
import agent from '../agent';
import {
  USERS_PAGE_LOADED,
  USERS_PAGE_UNLOADED,
  //USERS_PAGE_UPDATE_USER
  //USERS_PAGE_UPDATE_NAME

} from '../constants/actionTypes';

const mapStateToProps = state => ({
  ...state.users,
  currentUser: state.common.currentUser,
  isMobile: state.common.isMobile
});

const mapDispatchToProps = dispatch => ({
  onLoad: users =>
    dispatch({ type: USERS_PAGE_LOADED, payload: users }),
  onUnload: () =>
    dispatch({ type: USERS_PAGE_UNLOADED }),

});

class Users extends React.Component {
  state = {}
  constructor() {
    super();
    this.changeUser = ev => this.setState({ user: ev.target.value });
    this.changeName = ev => this.setState({ name: ev.target.value });
  }
  abrirModalCambiarFecha = user => {
    if(user.expiration)
      this.setState({ fecha: moment(user.expiration) });
    else
      this.setState({ fecha: moment(), noExpira: true });
    this.setState({ openCambiarExpiracion: true, user: user.email });
  }
  cambiaFecha = () => {
    agent.Users.update_expiration({ user: this.state.user, fecha: this.state.noExpira? null: this.state.fecha.format('YYYY-DD-MM 00:00:00:000')}).then(r => {
      agent.Users.lista_usuarios().then(u => {
        this.props.onLoad(u);
        this.setState({ openCambiarExpiracion: false });
      });
    })
  }
  agregaUser = () => {
    agent.Auth.add_user({ email: this.state.user, name: this.state.name, fecha: this.state.noExpira? null: this.state.fecha.format('YYYY-DD-MM 00:00:00:000')}).then(() => {
      agent.Users.lista_usuarios().then(u => {
        this.props.onLoad(u);
        this.setState({ openNuevoUsuario: false });
      });
    })
  }
  passwordReset = () => {
    if(this.state.reseteado) {
      this.setState({ confirm: false });
      return;
    }
    else {
      agent.Auth.new_password(this.state.user).then(r => {
        agent.Users.lista_usuarios().then(u => {
          this.props.onLoad(u);
          this.setState({ modalContent: "La contraseña del usuario: '" + this.state.user + "' ha sido restablecida a '123'", reseteado: true  });
        });
      })
    }
  }

  grantMobileAccess = () => 
    agent.Auth.toggle_mobile_access(this.state.user).then(r => 
      agent.Users.lista_usuarios().then(u => {
        this.props.onLoad(u);
        this.setState({ confirm: false });
      })
    );

  nuevoUsuarios() {
    this.setState({ openNuevoUsuario: true, fecha: moment(), noExpira: false, user: '', name: '', password: 123 });
  }

  deleteUser(user, confirmed) {
    if(!confirmed) 
      this.setState({
        confirm: true, 
        modalHeader: 'Confirmar', 
        modalContent: "¿Eliminar el usuario: '" + user.email + "'?",
        user: user.email,
        user_id: user.id,
        action: user_id => this.deleteUser(user_id, true)
      });
    else
    {
      agent.Auth.deleteUser(this.state.user_id).then(r => {
        agent.Users.lista_usuarios().then(u => {
          this.props.onLoad(u);
          this.setState({ 
            reseteado: true, 
            modalHeader: 'Info', 
            modalContent: "El usuario '" + this.state.user + "' ha sido eliminado", 
            action: () => this.setState({ confirm: false })
          });
        });
     }).catch(() => this.setState({ confirm: false }));
    }
  }

  reset = user => ev => {
    if(!user.reseteado)
      this.setState({ 
        confirm: true,
        modalHeader: 'Confirmar', 
        modalContent: "¿Reestablecer contraseña para el usuario: '" + user.email + "'?", 
        user: user.email, 
        reseteado: false, 
        action: () => this.passwordReset() 
      })
    else
      this.setState({ confirm: true, modalHeader: 'Info', modalContent: "La contraseña del usuario: '" + user.email + "'' ya ha sido restablecida a '123'", reseteado: true })
  }
  toggleMobileAccess = user => ev => {
    if(!user.acceso_movil)
      this.setState({ 
        confirm: true, 
        modalHeader: 'Confirmar', 
        modalContent: "¿Permitir acceso desde dispositivos moviles al usuario: '" + user.email + "'?", 
        user: user.email, 
        acceso_movil: false,
        action: () => this.grantMobileAccess() 
      })
    else
      this.setState({ 
        confirm: true, 
        modalHeader: 'Confirmar', 
        modalContent: "¿Quitar acceso movíl al usuario '" + user.email + '?',
        deleteModal: true, // para mostrar boton de cancelar
        user: user.email, 
        acceso_movil: true,
        action: () => this.grantMobileAccess()
      })
  }
 
  componentWillMount() {
    if(this.props.isMobile || (this.props.currentUser && Number(this.props.currentUser.id) !== 1 && Number(this.props.currentUser.id) !== 35))
      window.location.replace(agent.FOLDER + '/');
    
    this.props.onLoad(agent.Users.lista_usuarios());
  }

  componentWillUnmount() {
    this.props.onUnload();
  }
 
  render() {
    const { usuarios } = this.props;
    const { openCambiarExpiracion, openNuevoUsuario, fecha, user, name, password, confirm, modalHeader, modalContent, reseteado, noExpira, deleteModal, action } = this.state;
    if(!usuarios)
      return (
        <div>
          <Dimmer active page>
            <Loader content='Cargando lista de usuarios...' />
          </Dimmer>
        </div>
        );
    if(confirm)
      return (
        <Modal
          open={confirm}
          onClose={()=>{}}
          basic
          size='small'
        >
          <Header icon='browser' content={modalHeader} />
          <Modal.Content>
            <h3 style={{ color: '#e62525' }}>{modalContent}</h3>
          </Modal.Content>
          <Modal.Actions>
            {!reseteado || deleteModal? <Button onClick={() => this.setState({ confirm: false })} inverted>
              <Icon name='checkmark' />Cancelar
            </Button>: null}
            <Button color='green' onClick={action} inverted>
              <Icon name='checkmark' />Ok
            </Button>
          </Modal.Actions>
        </Modal>
      )
    return (
      <div style={{ marginBottom: '6em' }}>
        <div style={{ margin: '5em auto', width: '70%' }}>
          <Label as='a' color='teal' ribbon>Usuarios</Label>
          <Table basic='very' celled unstackable selectable style={{ marginTop: '3em', width: '100%' }}>
            <Table.Header>
              <Table.Row >
                <Table.HeaderCell style={{ textAlign: 'center'}}>Nombre</Table.HeaderCell>
                <Table.HeaderCell style={{ textAlign: 'center'}}>Usuario</Table.HeaderCell>
                <Table.HeaderCell style={{ textAlign: 'center', width: '7em'}}>Fecha de expiración</Table.HeaderCell>
                <Table.HeaderCell style={{ textAlign: 'center', width: '3em'}}>Contraseña</Table.HeaderCell>
                <Table.HeaderCell style={{ textAlign: 'center', width: '3em'}}>Acceso movíl</Table.HeaderCell>
                <Table.HeaderCell style={{ textAlign: 'center', width: '3em'}}>Eliminar</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>{ usuarios.map( r => 
              <Table.Row key={r.id} >
                <Table.Cell style={{ textAlign: 'center'}}>
                  <Popup
                    trigger={<div>{r.name}</div>}
                    content='Campo opcional, utilizado para identificar a la persona que utiliza la cuenta'
                  />
                </Table.Cell>
                <Table.Cell style={{ textAlign: 'center'}}>
                  <Popup
                    trigger={<div>{r.email}</div>}
                    content='username o email utilizado para iniciar sesión en el app'
                  />
                </Table.Cell>
                
                <Table.Cell style={{ textAlign: 'center', width: '7em'}}>
                  <Popup
                    trigger={<Label as='a' onClick={() => this.abrirModalCambiarFecha(r)}>
                      { r.expiration && r.expiration.indexOf(Number(1800)) === -1? 
                          <Moment date={r.expiration} format="DD/MM/YYYY"/>
                        : '- - - - -'
                      }</Label>}
                    content='Cambiar fecha de expiración'
                  />
                </Table.Cell>
                <Table.Cell style={{ textAlign: 'center', width: '3em' }}>
                  <Popup
                    trigger={<Button size='mini' color={!r.reseteado? 'black': 'instagram'} icon={r.reseteado? 'unlock': 'lock'} onClick={this.reset(r)} />}
                    content={r.reseteado? 'La contraseña de este usuario es 123':'Reestablecer contraseña'}
                  />
                </Table.Cell>
                <Table.Cell style={{ textAlign: 'center', width: '3em' }}>
                  <Popup
                    trigger={<Button size='mini' color={!r.acceso_movil? 'black': 'instagram'} icon={!r.acceso_movil? 'remove': 'checkmark'} onClick={this.toggleMobileAccess(r)} />}
                    content={!r.acceso_movil? 'Permitir acceso movíl':'Quitar acceso movíl'}
                  />
                </Table.Cell>
                <Table.Cell style={{ textAlign: 'center', width: '3em' }}>
                  <Popup
                    trigger={<Button size='mini' color='youtube' icon='remove' onClick={() => this.deleteUser(r, false)} disabled={Number(r.id) === 1}/>}
                    content='Eliminar usuario'
                  />
                </Table.Cell>
              </Table.Row> )}
            </Table.Body>
          </Table>
          <Button type="button" color='teal' onClick={() => this.nuevoUsuarios()}>Agregar usuario</Button>
        </div>

        <Modal dimmer={'blurring'} size={'small'} open={openCambiarExpiracion} onClose={this.closeCambiarExpiracion} style={{ height: '32em' }}>
          <Modal.Header>Cambiar fecha de expiración para el usuario <span style={{ color: 'red' }}>{user}</span></Modal.Header>
          <Modal.Content image style={{ minHeight: '23em' }}>
            <Form>
             { !noExpira? 
              <Form.Field>
                <label>Fecha de expiración</label>
                <SingleDatePicker
                  date={fecha}
                  isOutsideRange={() => false}
                  onDateChange={date => this.setState({ fecha: date })}
                  focused={this.state.focused} // PropTypes.bool
                  onFocusChange={({ focused }) => this.setState({ focused })} // PropTypes.func.isRequired
                />
              </Form.Field>: null }
              <Form.Field>
                <Checkbox label='Quitar fecha de expiración (la cuenta nunca expira)' checked={noExpira} onClick={()=> this.setState({noExpira: !noExpira})}/>
              </Form.Field>
            </Form>
          </Modal.Content>
          <Modal.Actions>
            <Button color='black' onClick={() => this.setState({openCambiarExpiracion: false})}>
              Cancelar
            </Button>
            <Button positive onClick={this.cambiaFecha}>
              Guardar
            </Button>
          </Modal.Actions>
        </Modal>

        <Modal dimmer={'blurring'} size={'small'} open={openNuevoUsuario} onClose={this.closeNuevoUsuario} style={{ height: '35em' }}>
          <Modal.Header>Agregar usuario</Modal.Header>
          <Modal.Content image style={{ minHeight: '26.7em' }}>
            <Form>
              <Form.Field>
                <label>Fecha de expiración</label>
                {!noExpira? 
                  <SingleDatePicker
                    date={fecha}
                    isOutsideRange={() => false}
                    onDateChange={date => this.setState({ fecha: date })}
                    focused={this.state.focused} // PropTypes.bool
                    onFocusChange={({ focused }) => this.setState({ focused })} // PropTypes.func.isRequired
                  />: null } <br/>
                <Checkbox label='Quitar fecha de expiración (la cuenta nunca expira)' checked={noExpira} onClick={()=> this.setState({noExpira: !noExpira})}/>
              </Form.Field>
              <Form.Field>
                <label>Nombre completo (opcional)</label>
                <input type="text" placeholder="Nombre" value={name} onChange={this.changeName} />
              </Form.Field>
              <Form.Field>
                <label>Usuario</label>
                <input type="text" placeholder="Usuario" value={user} onChange={this.changeUser} />
              </Form.Field>
              <Form.Field>
                <label>Contraseña</label>
                <input type="text" placeholder="Contraseña" value={password} disabled />
              </Form.Field>
            </Form>
          </Modal.Content>
          <Modal.Actions>
            <Button color='black' onClick={() => this.setState({openNuevoUsuario: false})}>
              Cancelar
            </Button>
            <Button positive onClick={() => this.agregaUser()} disabled={!user || user === ''}>
              Guardar
            </Button>
          </Modal.Actions>
        </Modal>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Users);