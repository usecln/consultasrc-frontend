import ListErrors from './ListErrors';
import React from 'react';
import agent from '../agent';
import { connect } from 'react-redux';
import { Button, Form, Label, Modal, Header, Icon } from 'semantic-ui-react'
import {
  UPDATE_FIELD_AUTH,
  LOGIN,
  LOGIN_PAGE_UNLOADED,
  APP_LOAD,
  TIME_NOT_ALLOWED
} from '../constants/actionTypes';

const mapStateToProps = state => ({ 
  ...state.auth, 
  ...state.common,
});

const mapDispatchToProps = dispatch => ({
  onLoad: (payload, token) => dispatch({ type: APP_LOAD, payload, token, skipTracking: true }),
  onChangeEmail: value => dispatch({ type: UPDATE_FIELD_AUTH, key: 'email', value }),
  onChangePassword: value => dispatch({ type: UPDATE_FIELD_AUTH, key: 'password', value }),
  onSubmit: payload => dispatch({ type: LOGIN, payload }),
  onUnload: () => dispatch({ type: LOGIN_PAGE_UNLOADED }),
  onTimeError: () => dispatch({ type: TIME_NOT_ALLOWED })
});
//this.setState({ modalHeader: 'Error', modalContent: e, modalOpen: true })
class Login extends React.Component {
  state = { modalOpen: false, outOfTime: false }
  constructor() {
    super();
    this.changeEmail = ev => this.props.onChangeEmail(ev.target.value);
    this.changePassword = ev => this.props.onChangePassword(ev.target.value);
    this.submitForm = (email, password) => ev => {
      ev.preventDefault();
      if(this.props.isMobile) {
        agent.Auth.login2(email, password).then(s => {
          if(s) {
            if(s.success) {
              let token = s.success.token;
              agent.setToken(token);
              this.props.onSubmit(s);
              this.props.onLoad(token ? agent.Auth.current() : null, token);
              window.localStorage.setItem('user', this.props.email);
              window.location.replace(agent.FOLDER + '/#/consulta_movil');
            }
            else if (s.response){
              window.localStorage.setItem('user', email);
              window.location.replace(agent.FOLDER + '/#/password_reset');
            }
            else if(s.error)
              this.setState({modalOpen: true, modalHeader: 'Error', modalContent: s.error.message})
          }
        });
      }
      else {
        agent.Auth.login(email, password, this.props.isMobile).then(s => {
          if(s) {
            if(s.success) {
              let token = s.success.token;
              agent.setToken(token);
              this.props.onSubmit(s);
              this.props.onLoad(token ? agent.Auth.current() : null, token);
              window.localStorage.setItem('user', this.props.email);
              window.location.replace(agent.FOLDER + '/#/consulta');
            }
            else if (s.response){
              window.localStorage.setItem('user', email);
              window.location.replace(agent.FOLDER + '/#/password_reset');
            }
            else if(s.error)
              this.setState({modalOpen: true, modalHeader: 'Error', modalContent: s.error.message})
          }
        });
      }
    };
  }
  componentWillMount() {
    if(this.props.currentUser)
      window.location.replace(agent.FOLDER + '/');
  }
  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
    const { email, password, isMobile }  = this.props;
    const { modalHeader, modalOpen, modalContent } = this.state;
    
    if(modalOpen) {
      return (
          <Modal open={modalOpen} onClose={this.handleClose} basic size='small'>
            <Header icon='browser' content={modalHeader} />
            <Modal.Content>
              <h3 style={{ color: '#e62525' }}>{modalContent}</h3>
            </Modal.Content>
            <Modal.Actions>
              <Button color='green' onClick={() => this.setState({modalOpen: false})} inverted>
                <Icon name='checkmark' />Ok
              </Button>
            </Modal.Actions>
          </Modal>
        );
    }
    else {
      return (
        <div style={{ backgroundColor: this.props.isMobile? '#efefef': 'white', bottom: '0', left: '0', position: 'fixed', right: '0', top: '0'}}>
          <div style={{height: '7em'}}/>
          <div style={{margin: 'auto', width: isMobile? '80%': '25%'}} >
            <Label as='a' color='teal' ribbon>Iniciar sesión</Label>
            <ListErrors errors={this.props.errors} />
            <Form style={{ marginTop: '3em' }} onSubmit={this.submitForm(email, password)}>
              <Form.Field>
                <label style={{ color: '#525252' }}>Usuario</label>
                <input type="text" placeholder="Usuario" value={email} onChange={this.changeEmail}/>
              </Form.Field>
              <Form.Field>
                <label style={{ color: '#525252' }}>Contraseña</label>
                <input type="password" placeholder="Contraseña" value={password} onChange={this.changePassword} />
              </Form.Field>
              <Button type="submit" color="teal" disabled={this.props.inProgress}>Entrar</Button>
            </Form>
          </div>
        </div>
      );
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
