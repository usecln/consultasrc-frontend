import agent from '../agent';
import Header from './Header';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { APP_LOAD, REDIRECT, MOBILE_DETECTED } from '../constants/actionTypes';

const mapStateToProps = state => ({
  appLoaded: state.common.appLoaded,
  appName: state.common.appName,
  currentUser: state.common.currentUser,
  redirectTo: state.common.redirectTo,
  isMobile: state.common.isMobile,
  outOfTime: state.common.outOfTime
});

const mapDispatchToProps = dispatch => ({
  onLoad: (payload, token) => dispatch({ type: APP_LOAD, payload, token, skipTracking: true }),
  onRedirect: () =>
    dispatch({ type: REDIRECT }),
  onMobileDetected: height => 
    dispatch({ type: MOBILE_DETECTED, payload: { height: height } })
});

class App extends React.Component {
  componentWillReceiveProps(nextProps) {
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      let body = document.body,
      html = document.documentElement;
      let height = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight );
      this.props.onMobileDetected(height);
    }
    if (nextProps.redirectTo) {
      this.context.router.replace(nextProps.redirectTo);
      this.props.onRedirect();
    }
  }

  componentWillMount() {
    const token = window.localStorage.getItem('jwt');
    if (token)
      agent.setToken(token);
    this.props.onLoad(token ? agent.Auth.current() : null, token);
  }

  render() {
    const { outOfTime } = this.props;
    if (this.props.appLoaded) {
      return (
        <div className="home-page">
          <Header
            appName={this.props.appName}
            currentUser={this.props.currentUser}
            isMobile={this.props.isMobile} />
          {this.props.children}
          { !this.props.isMobile && !outOfTime? 
            <footer style={{
              backgroundColor: '#efefef', // 'rgb(255, 123, 10)', //,
              color: '#a5a5a5',
              position: 'fixed',
              left: '0',
              bottom: '0',
              width: '100%',
              height: '5em',
            }}><br/>
              <div style={{ marginLeft: '3em' }}>
                <strong>{/*<span style={{ color: 'black' }}>SISTEMA EN MANTENIMIENTO <small>(MARQUE (667) 388 7971 EN CASO DE ERROR)</small></span>*/}Sistema de consultas para la secretaria de relaciones exteriores.</strong><br/>
                <strong style={{ color: '#2185d0' }}>Horario de consultas 7am a 4pm</strong>
              </div>
            </footer> : null }
        </div>
      );
    }
    return (
      <div className="home-page">
        <Header
          appName={this.props.appName}
          currentUser={this.props.currentUser} />
      </div>
    );
  }
}

App.contextTypes = {
  router: PropTypes.object.isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
