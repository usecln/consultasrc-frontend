import ListErrors from './ListErrors';
import React from 'react';
import { Button, Form, Label, Modal, Header, Icon } from 'semantic-ui-react'
import agent from '../agent';
import { connect } from 'react-redux';
import {
  PASSWORD_RESET_LOADED,
  PASSWORD_RESET_UNLOADED,
  PASSWORD_RESET_USER_UPDATED,
} from '../constants/actionTypes';

const mapStateToProps = state => ({
  ...state.password_reset,
  currentUser: state.common.currentUser
});

const mapDispatchToProps = dispatch => ({
  /* onClickLogout: () => dispatch({ type: LOGOUT }), */
  onUserUpdate: () => ev => dispatch({ type: PASSWORD_RESET_USER_UPDATED, payload: { name: ev.target.name, value: ev.target.value} }),
  onUserInit: user => dispatch({ type: PASSWORD_RESET_USER_UPDATED, payload: { name: "email", value: user } }),
  onLoad: () => dispatch({ type: PASSWORD_RESET_LOADED }),
  onUnload: () => dispatch({ type: PASSWORD_RESET_UNLOADED })
});

class PasswordReset extends React.Component {
  state = { modalOpen: false, logeado: false, password: '' }
  componentWillMount() {
    this.props.onLoad();
    var user = window.localStorage.getItem('user');
    if (user) {
      this.props.onUserInit(user);
      let jwt = window.localStorage.getItem('jwt');
      if(!jwt)
        window.localStorage.removeItem('user');
    }
    else {
      window.location.replace(agent.FOLDER + '/#/login');
    }
  }
  componentWillUnmount() {
    this.props.onUnload();
  }
  changePassword() {

    if(Number(this.props.password) === 123) {
      this.setState({ modalHeader: 'Error', modalContent: 'La contraseña no puede ser 123', modalOpen: true });
      return;
    }
    let next = () => {
      agent.Auth.reset_password({ email: this.props.email, password: this.props.password }).then(r => {
        if(r.response) {
          window.localStorage.clear();
          this.setState({ modalHeader: 'Contraseña actualizada', modalContent: 'La contraseña ha sido cambiada, ya puede iniciar sesión con la nueva contraseña', modalOpen: true });
        }
        if(r.error) 
          this.setState({ modalHeader: 'Error', modalContent: r.error.message, modalOpen: true });
        else {
          //this.setState({ modalHeader: 'Error', modalContent: 'Ha ocurrido un error', modalOpen: true });
        }
      });
    };
    let jwt = window.localStorage.getItem('jwt');
    let user = window.localStorage.getItem('user');
    if(jwt){
      this.setState({ logeado: true });
      agent.Auth.new_password(user).then(() => next());
    }
    else 
      next();
    
  }
  render() {
    const { email, password } = this.props;
    const { modalHeader, modalOpen, modalContent, logeado } = this.state;
    return (
      <div style={{ margin: '8em auto', width: '25%'}}>
        <div className="col-md-6 offset-md-3 col-xs-12">
          <Label as='a' color='red' ribbon>Cambiar contraseña</Label>
          <ListErrors errors={this.props.errors}></ListErrors>
          <Form style={{ marginTop: '3em' }} >
            <Form.Field>
              <label>Usuario</label>
              <input placeholder='Usuario' name="email" value={email} disabled/>
            </Form.Field>
            <Form.Field>
              <label>Nueva contraseña</label>
              <input type="password" placeholder="Nueva contraseña" name="password" value={password} onChange={this.props.onUserUpdate()}/>
            </Form.Field>
            <Form.Group inline>
              <Form.Field control={Button} color="instagram" type="submit" disabled={!password || (password && password.length === 0)} onClick={() => this.changePassword()} >Cambiar contraseña</Form.Field>
              {logeado? <Form.Field control={Button} type="button" onClick={() => window.location.replace(agent.FOLDER + '/#/login')}>Ir a Iniciar sesión</Form.Field>: null}
            </Form.Group>
          </Form>
          <Modal open={modalOpen} onClose={this.handleClose} basic size='small'>
            <Header icon='browser' content={modalHeader} />
            <Modal.Content>
              <h3 style={{ color: '#e62525' }}>{modalContent}</h3>
            </Modal.Content>
            <Modal.Actions>
              <Button color='green' onClick={() => {
                  this.setState({modalOpen: false});  
                  if(Number(password) !== 123) 
                    window.location.replace(agent.FOLDER + '/#/login'); 
                }} inverted>
                <Icon name='checkmark' />Ok
              </Button>
            </Modal.Actions>
          </Modal>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PasswordReset);