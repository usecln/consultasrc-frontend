import React from 'react';
import Moment from 'react-moment';
import { Button, Form, Input, Table, Header, Modal, Dimmer, Loader, Icon, Dropdown, Menu, Segment } from 'semantic-ui-react'
import agent from '../agent';
import { connect } from 'react-redux';
import {
  CONSULTA_MOVIL_PAGE_UNLOADED,
  ASYNC_START,
  ASYNC_END
} from '../constants/actionTypes';

const mapStateToProps = state => ({ 
  currentUser: state.common.currentUser, 
  inProgress: state.settings.inProgress,
  isMobile: state.common.isMobile,
  windowHeight: state.common.windowHeight
});

const mapDispatchToProps = dispatch => ({
  onUnload: () => dispatch({ type: CONSULTA_MOVIL_PAGE_UNLOADED }),
  onSaveStarted: () => dispatch({ type: ASYNC_START }),
  onSaveCanceled: () => dispatch({ type: ASYNC_END })
});
const opciones = [
  {
    opcion: 1,
    busqueda: 3 // detalles de nacimientos
  },
  {
    opcion: 2,
    busqueda: 4 // detalles de defunciones
  },
  {
    opcion: 3,
    busqueda: 5 // detalles de matrimonios
  },
  {
    opcion: 4,
    busqueda: 6 // detalles de divorcios
  }
];
class ConsultaMovil extends React.Component {
  state = { resultados: [], busqueda : '1', fecha: '', nombre: '', primer_apellido: '', segundo_apellido:'', open: false, modalConfirm: false }
  
  componentWillMount() {
    let r = localStorage.getItem('reload');
    if(r) {
      if(!this.props.currentUser) 
        window.location.replace('/#/login');
      localStorage.setItem('reload', false);
    }
    if(!this.props.currentUser) {
      localStorage.setItem('reload', true);
      window.location.reload();
    }
    if(!this.props.isMobile)
      window.location.replace(agent.FOLDER + '/#/consulta');
  }
  componentWillUnmount() {
    this.props.onUnload();
  }
  confirmMessage = message => this.setState({ confirm: message, modalConfirm: true });
  show = () => () => this.setState({ open: true });
  close = () => this.setState({ open: false });
  detalles = busqueda => ev => {
    this.props.onSaveStarted();
    let obj = {
      busqueda: opciones.filter(o => Number(o.opcion) === Number(this.state.busqueda))[0].busqueda,
      cve_oficialia: busqueda.CVE_OFICIALIA,
      ano: busqueda.ANO,
      tramite: busqueda.TRAMITE,
      servicio: busqueda.SERVICIO
    }
    agent.Consulta.buscar(obj).then(r => {
      this.setState({ detalles: r, open: true });
      this.props.onSaveCanceled();
    }).catch(() => this.props.onSaveCanceled());
  }
  handleBusqueda = (e, { value }) => this.setState({ busqueda: value })
  handleFecha = (e, { value }) => {
    if(this.state.fecha.length > value.length) {
      this.setState({ fecha: this.state.fecha.substring(0, this.state.fecha.length - 1) })
      return;
    }

    switch(value.length) {
      case 2:
      case 5:
        this.setState({ fecha: value + '/' })
        break;
      case 10:
        if(! /^(0[1-9]|[12]\d|3[01])\/(0[1-9]|1[0-2])\/[12]\d{3}$/.test(value) ) {
          this.confirmMessage('Fecha invalida, intente otra vez');
          this.setState({ fecha: '' });
        }
        else {
          if(value.length < 11)
            this.setState({ fecha: value })
        }
        break;
      default:
        if(value.length < 11)
          this.setState({ fecha: value })
      break;
    }
  }

  formClean = () => this.state.fecha === '' && this.state.nombre === '' && this.state.primer_apellido === '' && this.state.segundo_apellido === '';
  formValid = () => ( !this.state.fecha
    && (this.state.nombre || this.state.primer_apellido || this.state.segundo_apellido || this.state.fecha) ) || (this.state.fecha && /^(0[1-9]|[12]\d|3[01])\/(0[1-9]|1[0-2])\/[12]\d{3}$/.test(this.state.fecha));
  cleanForm = () => ev => this.setState({ resultados: [], busqueda : '1', fecha: '', nombre: '', primer_apellido: '', segundo_apellido:'', open: false, detalles: null });
  buscar = () => ev => {
    this.props.onSaveStarted();

    let obj = {
      busqueda: this.state.busqueda,
      sexo: this.state.sexo,
      fecha: this.state.fecha,
      nombre: this.state.nombre,
      primer_apellido: this.state.primer_apellido,
      segundo_apellido: this.state.segundo_apellido
    }
    agent.Consulta.busqueda_movil(obj).then(r => {
      this.setState({ resultados: r})
      if(this.state.resultados.length === 0)
        this.confirmMessage('No se encontraron resultados con esta información');
      this.props.onSaveCanceled();
    }).catch(() => this.props.onSaveCanceled());
  }

  render() {
    const { detalles, busqueda, fecha, nombre, primer_apellido, segundo_apellido, resultados, open, modalConfirm, confirm } = this.state;
    return (
      <div>
        <Dimmer active={this.props.inProgress} inverted page>
          <Loader size='medium'></Loader>
        </Dimmer>
        <Menu fixed='top' fluid>
          {resultados.length > 0? <Menu.Item link name='back' onClick={this.cleanForm()}><Icon link name='arrow left' /></Menu.Item>: null}
        </Menu>
        <div style={{ marginTop:'3em', width: '100%' }}>
          <Form style={{ margin: '0 auto', width: '90%' }}>
            { resultados.length === 0? 
              <div>
                <br/>
                <Form.Group widths='equal'>
                  <Form.Field control={Dropdown} label='Tipo de acta' selection options={[
                    { key: '1', text: 'Nacimientos', value: '1' },
                    { key: '2', text: 'Defunciones', value: '2' },
                    { key: '3', text: 'Matrimonios', value: '3' },
                    { key: '4', text: 'Divorcios', value: '4' }
                  ]} value={busqueda} onChange={this.handleBusqueda}/>
                  <Form.Field control={Input} label={'Fecha de ' + (Number(busqueda) === 1? 'nacimiento': 'registro') } value={fecha} placeholder='dd/mm/aaaa' onChange={this.handleFecha} />
                </Form.Group>
                <Form.Group widths='equal'>
                  <Form.Field control={Input} label='Nombre' placeholder='Nombre' value={nombre} onChange={(e, { value }) => this.setState({ nombre: value })}/>
                  <Form.Field control={Input} label='Primer apellido' placeholder='Primer apellido' value={primer_apellido} onChange={(e, { value }) => this.setState({ primer_apellido: value })}/>
                  <Form.Field control={Input} label='Segundo apellido' placeholder='Segundo apellido' value={segundo_apellido} onChange={(e, { value }) => this.setState({ segundo_apellido: value })}/>
                </Form.Group>
                <div>
                  <Button primary disabled={!this.formValid()} onClick={this.buscar()} hidden={resultados.length !== 0}>Buscar</Button>
                  <Button secondary disabled={this.formClean()} onClick={this.cleanForm()}>{resultados.length > 0? 'Nueva busqueda' : 'Limpiar pantalla'}</Button>
                </div>
                <br/>
              </div> : null }
            { resultados.length > 0? 
              <div>
                <br/>
                <div>
                  <Segment>
                    <Table basic='very' celled unstackable selectable>
                      <Table.Body>
                        {nombre?
                        <Table.Row>
                          <Table.Cell style={{ textAlign: 'left'}}>
                            <Header as='h4' image>
                              Nombre
                            </Header>
                          </Table.Cell>
                          <Table.Cell style={{ textAlign: 'left'}}>
                            {nombre}
                          </Table.Cell>
                        </Table.Row>: null}
                        {primer_apellido?
                        <Table.Row>
                          <Table.Cell style={{ textAlign: 'left'}}>
                            <Header as='h4' image>
                              Primer apellido
                            </Header>
                          </Table.Cell>
                          <Table.Cell style={{ textAlign: 'left'}}>
                            {primer_apellido}
                          </Table.Cell>
                        </Table.Row>: null}
                        {segundo_apellido?
                        <Table.Row>
                          <Table.Cell style={{ textAlign: 'left'}}>
                            <Header as='h4' image>
                              Segundo apellido
                            </Header>
                          </Table.Cell>
                          <Table.Cell style={{ textAlign: 'left'}}>
                            {segundo_apellido}
                          </Table.Cell>
                        </Table.Row>: null}
                        {fecha?
                        <Table.Row>
                          <Table.Cell style={{ textAlign: 'left'}}>
                            <Header as='h4' image>
                              Fecha
                            </Header>
                          </Table.Cell>
                          <Table.Cell style={{ textAlign: 'left'}}>
                            {fecha}
                          </Table.Cell>
                        </Table.Row>
                        : null}
                      </Table.Body>
                    </Table>

                  </Segment>
                  <br/><small style={{ color: '#666' }}>Haga clic en un renglon de la tabla para ver mas detalles</small><br/><br/>
                  <small style={{ color: 'red' }}>{resultados.length + ' registros encontrados'}</small><br/><br/>
                  <Table basic='very' celled unstackable selectable style={{ margin: '0em auto', marginBottom: '6em', width: '100%' }}>
                    <Table.Header>
                      <Table.Row>
                        <Table.HeaderCell style={{ textAlign: 'center'}}>Nombre</Table.HeaderCell>
                        <Table.HeaderCell style={{ textAlign: 'center'}}>Fecha de {Number(busqueda) === 1? 'nacimiento': 'registro'}</Table.HeaderCell>
                      </Table.Row>
                    </Table.Header>
                    <Table.Body>
                      { resultados.map( r => 
                      <Table.Row key={Number(r.CVE_OFICIALIA + r.TRAMITE + r.SERVICIO + r.ANO) + '-' + Math.random()} onClick={this.detalles(r)}>
                        <Table.Cell style={{ color: 'rgba(0, 0, 0, 0.82)' }}>
                          <strong>
                            <Icon name={r.SEXO === 'F'? 'female': (r.SEXO === 'M' ? 'male': 'question') } size='large'/>
                            <div style={{ marginLeft: '2em', marginTop: '-1.6em' }}>
                              {r.NOMBRE}<br/>
                              <span style={{ float: 'right', color: Number(r.STATUS) < 30? '#2185d0': 'red' }}>
                                <div style={{ textAlign: 'center' }}>
                                  {r.STATUS}<br/>
                                  <small>status</small>
                                </div>
                              </span>
                              {r.PRIMER_APELLIDO + ' ' + r.SEGUNDO_APELLIDO}
                            </div>
                          </strong>
                            { r.LOCALIDAD? <small style={{ marginLeft: '2.5em' }}><strong>LOCALIDAD: </strong>{r.LOCALIDAD}</small>: null }
                        </Table.Cell>
                        <Table.Cell style={{ textAlign: 'center', color: 'rgba(0, 0, 0, 0.82)' }}>
                          { 
                            Number(busqueda) === 1?
                            <Moment date={r.FECHA_NACIMIENTO} format="DD/MM/YYYY"/> :
                            <Moment date={r.FECHA_REGISTRO} format="DD/MM/YYYY"/>
                          }
                        </Table.Cell>
                      </Table.Row> )}
                    </Table.Body>
                  </Table>
                </div>
              </div>
              : null
            }
          </Form>
          <Modal
            open={modalConfirm}
            onClose={this.handleClose}
            basic
            size='small'
          >
            <Header icon='browser' content='Verifique la información proporcionada' />
            <Modal.Content>
              <h3 style={{ color: '#e62525' }}>{confirm}</h3>
            </Modal.Content>
            <Modal.Actions>
              <Button color='green' onClick={() => this.setState({confirm: null, modalConfirm: false})} inverted>
                <Icon name='checkmark' />Ok
              </Button>
            </Modal.Actions>
          </Modal>
          <Modal dimmer={'blurring'} size={'fullscreen'} open={open} onClose={this.close} >
            <Modal.Content>
              { detalles && detalles.nacionalidad? 
                <Table basic='very' celled unstackable style={{ width: '100%' }}>
                  <Table.Body>
                   <Table.Row>
                     <Table.Cell>
                        <div style={{ marginLeft: '2em' }}>
                          <strong>{ detalles.conyuge2? 'PRIMER CONYUGE' : (detalles.SEXO === 'M' || !detalles.SEXO? 'EL REGISTRADO' : 'LA REGISTRADA')}</strong><br/>
                          { !detalles.conyuge2 ? <Icon name={detalles.SEXO === 'F'? 'female': (detalles.SEXO === 'M' ? 'male': 'question') } />: null }
                          {detalles.NOMBRE}{' '}
                          {detalles.PRIMER_APELLIDO}{' '}
                          {detalles.SEGUNDO_APELLIDO}
                          <span style={{ float: 'right', color: Number(detalles.STATUS) < 30? '#2185d0': 'red'  }}>
                            <div style={{ textAlign: 'center' }}>
                              {detalles.STATUS}<br/>
                              <small>status</small>
                            </div>
                          </span>
                          <br/>
                          <small>
                            <strong>NACIONALIDAD: </strong>{detalles.nacionalidad.NACIONALIDAD}
                          </small>
                        </div>
                      </Table.Cell>
                    <Table.Cell />
                  </Table.Row>
                  { 
                    detalles.conyuge2?
                    <Table.Row>
                      <Table.Cell>
                        <div style={{ marginLeft: '2em' }}>
                          <strong>SEGUNDO CONYUGE</strong><br/>
                          {detalles.conyuge2.NOMBRE}{' '}
                          {detalles.conyuge2.PRIMER_APELLIDO}{' '}
                          {detalles.conyuge2.SEGUNDO_APELLIDO}
                          <br/>
                          <small>
                            <strong>NACIONALIDAD: </strong>{detalles.conyuge2.nacionalidad.NACIONALIDAD}
                          </small>
                        </div>
                      </Table.Cell>
                    </Table.Row>: null
                  }
                  {
                    detalles.DETALLES.map(d => 
                    <Table.Row key={d.tipo_detalle.NOM_DETALLE}>
                      <Table.Cell>
                        <div style={{ marginLeft: '2em' }}>
                          <strong>{d.tipo_detalle.NOM_DETALLE}</strong><br/>
                          {d.NOMBRE}{' '}
                          {d.PRIMER_APELLIDO}{' '}
                          {d.SEGUNDO_APELLIDO}
                          <br/>
                          <small>
                            <strong>NACIONALIDAD: </strong>{d.nacionalidad.NACIONALIDAD}{' '}
                            <strong>EDAD: </strong>{d.EDAD}
                          </small>
                        </div>
                      </Table.Cell>
                    </Table.Row>)
                  }
                  </Table.Body>
                </Table>: <strong>NO TIENE DETALLES</strong>}
            </Modal.Content>
            <Modal.Actions>
              <Button color='black' onClick={this.close}>
                Cerrar
              </Button>
            </Modal.Actions>
          </Modal>
        </div>
  </div>
  )}
}

export default connect(mapStateToProps, mapDispatchToProps)(ConsultaMovil);
