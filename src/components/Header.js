import React from 'react';
import agent from '../agent';
import { Link } from 'react-router';
import { Image, Menu, Icon } from 'semantic-ui-react'

const LoggedInView = props => {
  if (props.currentUser.currentUser) {
    if(props.currentUser.isMobile)
      return (  
        <Menu fixed='bottom' icon fluid widths={2}>
          <Menu.Item name='gamepad' active={props.activeItem === 'gamepad'} as={Link} to=""
              onClick={() => window.location.replace(agent.FOLDER + '/#/consulta_movil')}>
            <Icon name='search' />
            consultas
          </Menu.Item>

          <Menu.Item name='salir' active={props.activeItem === 'salir'} as={Link} to="" onClick={() => { 
                window.localStorage.clear(); 
                window.location.replace(agent.FOLDER + '/#/login'); 
              }}>
            <Icon name='power' />
            Salir
          </Menu.Item>
        </Menu>
      );
    return (
      <div>
        <Menu fixed='top' size='mini'>
          <Menu.Item style={{ marginLeft: '10em' }} />
          <Menu.Item position='left'>
            <Image size='mini' src={'http://www.androidpolice.com/wp-content/uploads/2016/02/nexus2cee_docs_suite_transparent_small.png'} style={{ marginRight: '1.5em', width: '2em' }} />
            Consultas registro civil
          </Menu.Item>
          <Menu.Menu position='right' style={{ marginRight: '7em' }}>
            { 
              [1].indexOf(Number(props.currentUser.currentUser.id)) !== -1? 
              <Menu.Item as={Link} to="users" name='Usuarios'/> : null
            }
            <Menu.Item as={Link} to="consulta" name='Consultas'/>
            <Menu.Item as={Link} to="settings">Cambiar contraseña</Menu.Item>
            <Menu.Item as={Link} to=""
              onClick={() => { 
                window.localStorage.clear(); 
                window.location.replace(agent.FOLDER + '/#/login'); 
              }
            }>
              <span style={{ color: 'red' }}>{'Salir'}</span>
            </Menu.Item>
          </Menu.Menu>
        </Menu>
      </div>
    );
  }

  return null;
};

class Header extends React.Component {
  state = { activeItem: 'gamepad' }

  handleItemClick = (e, { name }) => this.setState({ activeItem: name })
  render() {
    return (
      <div>
        <LoggedInView currentUser={this.props} activeItem={this.state.activeItem} />
      </div>
    );
  }
}

export default Header;
