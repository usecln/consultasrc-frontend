import React from 'react';
import { connect } from 'react-redux';
import agent from '../agent';
import {
  HOME_PAGE_LOADED,
  HOME_PAGE_UNLOADED,
} from '../constants/actionTypes';

const mapStateToProps = state => ({
  ...state.home,
  appName: state.common.appName,
  token: state.common.token,
  isMobile: state.common.isMobile,
  currentUser: state.common.currentUser,
});

const mapDispatchToProps = dispatch => ({
  onLoad: () =>
    dispatch({ type: HOME_PAGE_LOADED }),
  onUnload: () =>
    dispatch({  type: HOME_PAGE_UNLOADED })
});

class Home extends React.Component {
  
  componentWillMount() {
    if(!this.props.currentUser)
      window.location.replace(agent.FOLDER + '/#/login');
    else {
      if(this.props.isMobile)
        window.location.replace(agent.FOLDER + '/#/consulta_movil');
      window.location.replace(agent.FOLDER + '/#/consulta');
    }
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
    const { isMobile } = this.props;
    return (
      <div className="home-page" style={{ backgroundColor: '#272121' }}>
        
        <div className="container page">
          { isMobile? null :
            <div className="row">
              Sistema de consultas para la secretaria de relaciones exteriores
            </div>
          }
        </div>

      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);