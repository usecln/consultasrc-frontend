import React from 'react';
import Moment from 'react-moment';
import { Button, Form, Input, Table, Header, Modal, Dimmer, Loader, Icon, Card, Dropdown } from 'semantic-ui-react'
import printJS from 'print-js';
import agent from '../agent';
import { connect } from 'react-redux';
import {
  CONSULTA_PAGE_UNLOADED,
  ASYNC_START,
  ASYNC_END
} from '../constants/actionTypes';

const mapStateToProps = state => ({ 
  currentUser: state.common.currentUser, 
  inProgress: state.settings.inProgress,
  isMobile: state.common.isMobile
});

const mapDispatchToProps = dispatch => ({
  onUnload: () => dispatch({ type: CONSULTA_PAGE_UNLOADED }),
  onSaveStarted: () => dispatch({ type: ASYNC_START }),
  onSaveCanceled: () => dispatch({ type: ASYNC_END })
});

class Consulta extends React.Component {
  state = { resultados: [], busqueda : '1', sexo : '1', fecha: '', nombre: '', primer_apellido: '', segundo_apellido:'', open: false, modalConfirm: false }

  componentWillMount() {
    let r = localStorage.getItem('reload');
    if(r) {
      if(!this.props.currentUser)
        window.location.replace('/#/login');
      localStorage.setItem('reload', false);
    }
    if(!this.props.currentUser) {
      localStorage.setItem('reload', true);
      window.location.reload();
    }
    if(this.props.isMobile)
      window.location.replace(agent.FOLDER + '/#/consulta_movil');
  }
  componentWillUnmount() {
    this.props.onUnload();
  }
  
  print = () => {
    this.setState({ printing: true });
    agent.Consulta.generaPDF({ detalles: {...this.state.detalles}, busqueda: Number(this.state.busqueda) + 2 }).then(r => {
      this.setState({ printing: false });
      printJS({
        printable: agent.API_STATIC + '/comprobante/' + r.file,
        type: 'pdf',
        showModal: true,
        modalMessage: ''
      });
    });
  }
  download = () => {
    const { nombre, primer_apellido, segundo_apellido } = this.state;
    this.setState({ downloading: true });
    agent.Consulta.generaPDF(this.state.llave).then(r => {
      this.setState({ downloading: false });
      let n = '';
      n += nombre.length > 0? nombre: ''
      n += primer_apellido.length > 0? '_' + primer_apellido: '';
      n += segundo_apellido.length > 0? '_' + segundo_apellido: '';
      n += '.pdf';
      n = n.toLowerCase().replace(' ', '_');
      let link = document.createElement("a");
      link.download = n;
      link.href = 'data:application/pdf;base64,' + r.data;
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    });
  };
  confirmMessage = message => this.setState({ confirm: message, modalConfirm: true });
  show = () => () => this.setState({ open: true });
  close = () => this.setState({ open: false, printing: false });
  detalles = busqueda => ev => {
    let obj = {
      busqueda: Number(this.state.busqueda) === 1? 3: (Number(this.state.busqueda) === 2? 4: null),
      cve_oficialia: busqueda.CVE_OFICIALIA,
      ano: busqueda.ANO,
      tramite: busqueda.TRAMITE,
      servicio: busqueda.SERVICIO
    }
    
    this.setState({llave: obj});
    //busqueda.busqueda = Number(this.state.busqueda) === 1? 3: (Number(this.state.busqueda) === 2? 4: null);

    this.props.onSaveStarted();

    agent.Consulta.buscar(obj).then(r => {
      this.setState({ detalles: r, open: true });
      this.props.onSaveCanceled();
    }).then(() => this.props.onSaveCanceled());
  }
  handleBusqueda = (e, { value }) => this.setState({ busqueda: value })
  handleSexo = (e, { value }) => this.setState({ sexo: value })
  handleFecha = (e, { value }) => {
    if(this.state.fecha.length > value.length) {
      this.setState({ fecha: this.state.fecha.substring(0, this.state.fecha.length - 1) })
      return;
    }

    switch(value.length) {
      case 2:
      case 5:
        this.setState({ fecha: value + '/' })
        break;
      case 10:
        if(! /^(0[1-9]|[12]\d|3[01])\/(0[1-9]|1[0-2])\/[12]\d{3}$/.test(value) ) {
          this.confirmMessage('Fecha invalida, intente otra vez');
          this.setState({ fecha: '' });
        }
        else {
          if(value.length < 11)
            this.setState({ fecha: value })
        }
        break;
      default:
        if(value.length < 11)
          this.setState({ fecha: value })
      break;
    }
  }

  formClean = () => this.state.fecha === '' && this.state.nombre === '' && this.state.primer_apellido === '' && this.state.segundo_apellido === '';
  formValid = () => /^(0[1-9]|[12]\d|3[01])\/(0[1-9]|1[0-2])\/[12]\d{3}$/.test(this.state.fecha) && this.state.nombre !== '' && (this.state.primer_apellido !== '' || this.state.segundo_apellido !== '');
  cleanForm = () => ev => this.setState({ resultados: [], busqueda : '1', sexo : '1', fecha: '', nombre: '', primer_apellido: '', segundo_apellido:'', open: false });
  buscar = () => ev => {
    this.props.onSaveStarted();
    let obj = {
      busqueda: this.state.busqueda,
      sexo: this.state.sexo,
      fecha: this.state.fecha,
      nombre: this.state.nombre,
      primer_apellido: this.state.primer_apellido,
      segundo_apellido: this.state.segundo_apellido
    }
    agent.Consulta.buscar(obj).then(r => {
      this.setState({ resultados: r})
      if(this.state.resultados.length === 0) 
        this.confirmMessage('No se encontraron resultados con esta información');
      this.props.onSaveCanceled();
    }).catch(() => this.props.onSaveCanceled());
  }
  edadMutator = edad => Number(edad) === 0 || Number(edad) === 99 || Number(edad) === 999 ? '- - -': Number(edad);

  render() {
    const { busqueda, sexo, fecha, nombre, primer_apellido, segundo_apellido, resultados, detalles, open, modalConfirm, confirm, printing } = this.state
    return (
      <div>
        <Dimmer active={this.props.inProgress} inverted>
          <Loader size='medium'></Loader>
        </Dimmer>
        <Card raised style={{ backgroundColor: '#ababab2b', margin: '10em auto', position: 'static', width: '80%'}}>
          <Card.Content header={'Búsqueda de actas de ' + (Number(busqueda) === 1? 'nacimiento': 'defunción')} />
          <Card.Content description={
        <div>
          <Form style={{ position: 'relative', width: '100%' }}>
            { resultados.length === 0? 
              <div>
                <Form.Group widths='equal'>
                  <Form.Field control={Dropdown} label='Tipo de acta' selection options={[
                    { key: '1', text: 'Nacimientos', value: '1' },
                    { key: '2', text: 'Defunciones', value: '2' }
                  ]} value={busqueda} onChange={this.handleBusqueda}/>
                  <Form.Field control={Dropdown} label='Sexo' selection options={[
                    { key: '1', text: 'Masculino', value: '1' },
                    { key: '2', text: 'Femenino', value: '2' }
                  ]} value={sexo} onChange={this.handleSexo}/>
                  <Form.Field control={Input} label={'Fecha de ' + (Number(busqueda) === 1? 'nacimiento': 'registro') } value={fecha} placeholder='dd/mm/aaaa' onChange={this.handleFecha} />
                </Form.Group>
                <Form.Group widths='equal'>
                  <Form.Field control={Input} label='Nombre' placeholder='Nombre' value={nombre} onChange={(e, { value }) => this.setState({ nombre: value })}/>
                  <Form.Field control={Input} label='Primer apellido' placeholder='Primer apellido' value={primer_apellido} onChange={(e, { value }) => this.setState({ primer_apellido: value })}/>
                  <Form.Field control={Input} label='Segundo apellido' placeholder='Segundo apellido' value={segundo_apellido} onChange={(e, { value }) => this.setState({ segundo_apellido: value })}/>
                </Form.Group>
              </div> : null }
            { resultados.length > 0? 
              <Table basic='very' celled unstackable style={{ width: '100%' }}>
                <Table.Header style={{ backgroundColor: 'rgba(53, 127, 183, 0.4)' }}>
                  <Table.Row>
                    <Table.HeaderCell style={{ textAlign: 'center'}}>Nombre</Table.HeaderCell>
                    <Table.HeaderCell style={{ textAlign: 'center'}}>Primer apellido</Table.HeaderCell>
                    <Table.HeaderCell style={{ textAlign: 'center'}}>Segundo apellido</Table.HeaderCell>
                    <Table.HeaderCell style={{ textAlign: 'center', width: '4em'}}>Sexo</Table.HeaderCell>
                    <Table.HeaderCell style={{ textAlign: 'center', width: '7em'}}>Fecha de nacimiento</Table.HeaderCell>
                    <Table.HeaderCell style={{ textAlign: 'center', width: '7em'}}>Fecha de registro</Table.HeaderCell>
                    <Table.HeaderCell style={{ textAlign: 'center'}}>Localidad</Table.HeaderCell>
                    <Table.HeaderCell style={{ textAlign: 'center', width: '5em'}}>Número de acta</Table.HeaderCell>
                    <Table.HeaderCell style={{ width: '5.4em' }}>Detalles</Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
                <Table.Body style={{ backgroundColor: 'rgba(0, 192, 255, 0.14)' }}>{ resultados.map( r => 
                  <Table.Row key={Number(r.cve_oficialia + r.tramite + r.servicio + r.ano)} >
                    <Table.Cell style={{ textAlign: 'center'}}>
                      {r.NOMBRE? r.NOMBRE : '- - - - -'}
                    </Table.Cell>
                    <Table.Cell style={{ textAlign: 'center'}}>
                      {r.PRIMER_APELLIDO? r.PRIMER_APELLIDO: '- - - - -'}
                    </Table.Cell>
                    <Table.Cell style={{ textAlign: 'center'}}>
                      {r.SEGUNDO_APELLIDO? r.SEGUNDO_APELLIDO: '- - - - -'}
                    </Table.Cell>
                    <Table.Cell style={{ textAlign: 'center', width: '4em'}}>
                      {r.SEXO? r.SEXO: '- - -'}
                    </Table.Cell>
                    <Table.Cell style={{ textAlign: 'center', width: '7em'}}>
                      { r.FECHA_NACIMIENTO.indexOf(Number(1800)) === -1? 
                          <Moment date={r.FECHA_NACIMIENTO} format="DD/MM/YYYY"/>
                        : '- - - - -'
                      }
                    </Table.Cell>
                    <Table.Cell style={{ textAlign: 'center', width: '7em'}}>
                      { r.FECHA_REGISTRO.indexOf(Number(1800)) === -1? 
                          <Moment date={r.FECHA_REGISTRO} format="DD/MM/YYYY"/>
                          : '- - - - -'
                      }
                    </Table.Cell>
                    <Table.Cell style={{ textAlign: 'center'}}>
                      {r.LOCALIDAD? r.LOCALIDAD: '- - - - -'}
                    </Table.Cell>
                    <Table.Cell style={{ textAlign: 'center', width: '5em'}}>
                      {r.ACTA}
                    </Table.Cell>
                    <Table.Cell style={{ textAlign: 'center', width: '3em' }}>
                       <Button size='mini' color='black' circular icon='file outline' onClick={this.detalles(r)} />
                    </Table.Cell>
                  </Table.Row> )}
                </Table.Body>
              </Table>
              : null
            }
          </Form>
          <Modal
            open={modalConfirm}
            onClose={this.handleClose}
            basic
            size='small'
          >
            <Header icon='browser' content='Verifique la información proporcionada' />
            <Modal.Content>
              <h3 style={{ color: '#e62525' }}>{confirm}</h3>
            </Modal.Content>
            <Modal.Actions>
              <Button color='green' onClick={() => this.setState({confirm: null, modalConfirm: false})} inverted>
                <Icon name='checkmark' />Ok
              </Button>
            </Modal.Actions>
          </Modal>
          <Modal dimmer={'blurring'} size={'fullscreen'} open={open} onClose={this.close} >
            <Modal.Header>Detalles del acta de {Number(busqueda) === 1? 'nacimiento': 'defunción'}</Modal.Header>
            { detalles && detalles.nacionalidad? 
              <Modal.Content>
                <Table basic='very' celled unstackable style={{ width: '100%' }}>
                  <Table.Header style={{ backgroundColor: 'rgba(8, 38, 41, 0.14)' }}>
                    <Table.Row style={{ height: '3em' }}> 
                      <Table.HeaderCell style={{ textAlign: 'center', width: '15em' }}>Persona</Table.HeaderCell>
                      <Table.HeaderCell style={{ textAlign: 'center'}}>Nombre</Table.HeaderCell>
                      <Table.HeaderCell style={{ textAlign: 'center'}}>Primer apellido</Table.HeaderCell>
                      <Table.HeaderCell style={{ textAlign: 'center'}}>Segundo apellido</Table.HeaderCell>
                      <Table.HeaderCell style={{ textAlign: 'center'}}>Nacionalidad</Table.HeaderCell>
                      <Table.HeaderCell style={{ textAlign: 'center'}}>Edad</Table.HeaderCell>
                    </Table.Row>
                  </Table.Header>
                  <Table.Body>
                    <Table.Row>
                      <Table.Cell style={{ textAlign: 'center', color: '#133a6d' }}>
                        <small><strong>{detalles.SEXO === 'M' || !detalles.SEXO? 'EL REGISTRADO' : 'LA REGISTRADA'}</strong></small>
                      </Table.Cell>
                      <Table.Cell style={{ textAlign: 'left'}}>
                        {detalles.NOMBRE}
                      </Table.Cell>
                      <Table.Cell style={{ textAlign: 'left'}}>
                        {detalles.PRIMER_APELLIDO}
                      </Table.Cell>
                      <Table.Cell style={{ textAlign: 'left'}}>
                        {detalles.SEGUNDO_APELLIDO}
                      </Table.Cell>
                      <Table.Cell style={{ textAlign: 'center'}}>
                        {detalles.nacionalidad.NACIONALIDAD}
                      </Table.Cell>
                      <Table.Cell />
                    </Table.Row>
                    { 
                      detalles.DETALLES.map(d => 
                      <Table.Row key={d.tipo_detalle.NOM_DETALLE}>
                        <Table.Cell style={{ textAlign: 'center', color: '#133a6d'  }}>
                          <small><strong>{d.tipo_detalle.NOM_DETALLE}</strong></small>
                        </Table.Cell>
                        <Table.Cell style={{ textAlign: 'left'}}>
                        {d.NOMBRE}
                      </Table.Cell>
                      <Table.Cell style={{ textAlign: 'left'}}>
                        {d.PRIMER_APELLIDO}
                      </Table.Cell>
                      <Table.Cell style={{ textAlign: 'left'}}>
                        {d.SEGUNDO_APELLIDO}
                      </Table.Cell>
                      <Table.Cell style={{ textAlign: 'center'}}>
                        {d.nacionalidad.NACIONALIDAD}
                      </Table.Cell>
                      <Table.Cell style={{ textAlign: 'center'}}>
                        {this.edadMutator(d.EDAD)}
                      </Table.Cell>
                      </Table.Row>)
                    }
                    
                  </Table.Body>
                </Table>
              </Modal.Content>:<strong>NO TIENE DETALLES</strong> }
            <Modal.Actions>
              <div>
                <Button icon labelPosition='left' onClick={this.print} disabled={printing}>
                  <Icon name='print' color='black'/>
                  {printing? 'Espere...' : 'Imprimir'}
                </Button>
                <Button color='black' onClick={this.close}>
                  Cerrar
                </Button>
              </div>
            </Modal.Actions>
          </Modal>
      </div>} />
      <Card.Content extra>
        { resultados.length === 0? <Button color="instagram" disabled={!this.formValid()} onClick={this.buscar()}>Buscar</Button>: null }
        <Button disabled={this.formClean()} onClick={this.cleanForm()}>{resultados.length > 0? 'Nueva busqueda' : 'Limpiar pantalla'}</Button>
      </Card.Content>
    </Card>
  </div>
  )}
}

export default connect(mapStateToProps, mapDispatchToProps)(Consulta);
