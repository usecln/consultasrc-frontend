import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import React from 'react';
import { Router, Route, IndexRoute, hashHistory } from 'react-router';
import store from './store';

import App from './components/App';
import Home from './components/Home';
import Pruebas from './components/Pruebas';
import Users from './components/Users';
import Login from './components/Login';
import Consulta from './components/Consulta';
import ConsultaMovil from './components/ConsultaMovil';
import PasswordReset from './components/PasswordReset';

ReactDOM.render((
  <Provider store={store}>
    <Router history={hashHistory}>
      <Route path="/" component={App}>
        <IndexRoute component={Home} />
        <Route path="users" component={Users} />
        <Route path="login" component={Login} />
        <Route path="pruebas" component={Pruebas} />
        <Route path="consulta" component={Consulta} />
        <Route path="consulta_movil" component={ConsultaMovil} />
        <Route path="settings" component={PasswordReset} />
        <Route path="password_reset" component={PasswordReset} />
      </Route>
    </Router>
  </Provider>
), document.getElementById('root'));
