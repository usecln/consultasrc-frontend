import auth from './reducers/auth';
import common from './reducers/common';
import home from './reducers/home';
import users from './reducers/users';
import password_reset from './reducers/password_reset';
import consulta from './reducers/consulta';
import profile from './reducers/profile';
import settings from './reducers/settings';

import { combineReducers } from 'redux';

export default combineReducers({
  auth,
  common,
  home,
  users,
  password_reset,
  consulta,
  profile,
  settings
});
