import { 
  PASSWORD_RESET_LOADED,
  PASSWORD_RESET_UNLOADED,
  PASSWORD_RESET_USER_UPDATED 
} from '../constants/actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case PASSWORD_RESET_LOADED:
      return { email: '', password: '' }
    case PASSWORD_RESET_USER_UPDATED:
      if(action.payload.name === 'password')
        return { ...state, password: action.payload.value }
      if(action.payload.name === 'email')
        return { ...state, email: action.payload.value }
      return state;
    case PASSWORD_RESET_UNLOADED:
      return state;
    default:
      return state;
  }
};
