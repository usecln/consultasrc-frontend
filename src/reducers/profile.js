import {
  PROFILE_PAGE_LOADED,
  PROFILE_PAGE_UNLOADED
} from '../constants/actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case PROFILE_PAGE_LOADED:
      // en payload esta el response del primer request (Profile)
      if(action.payload)
        return {
          ...action.payload.profile
        };
       return {

        };
    case PROFILE_PAGE_UNLOADED:
      return {};
    default:
      return state;
  }
};
