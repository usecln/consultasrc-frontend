import {
  APP_LOAD,
  MOBILE_DETECTED,
  REDIRECT,
  LOGOUT,
  SETTINGS_SAVED,
  LOGIN,
  REGISTER,  
  HOME_PAGE_UNLOADED,
  PROFILE_PAGE_UNLOADED,
  SETTINGS_PAGE_UNLOADED,
  LOGIN_PAGE_UNLOADED,
  REGISTER_PAGE_UNLOADED,
  TIME_NOT_ALLOWED
} from '../constants/actionTypes';

const defaultState = {
  appName: 'Actas USE',
  token: null,
  viewChangeCounter: 0
};

export default (state = defaultState, action) => {
  if(action.error || (action.payload && action.payload.error))
    window.location.replace('/#/login');
  switch (action.type) {
    case APP_LOAD:
      return {
        ...state,
        token: (action.payload && action.payload.success)? action.payload.success.token: null,
        appLoaded: true,
        currentUser: (action.payload && action.payload.success)? action.payload.success.user : null,
        outOfTime: false
      };
    case MOBILE_DETECTED:
      return {
        ...state,
        isMobile: true,
        windowHeight: action.payload.height
      }
    case TIME_NOT_ALLOWED:
      return {
        ...state,
        isMobile: true,
        outOfTime: true
      }
    case REDIRECT:
      return { ...state, redirectTo: null };
    case LOGOUT:
      return { ...state, redirectTo: '/', token: null, currentUser: null };
    case SETTINGS_SAVED:
      return {
        ...state,
        redirectTo: action.payload.error ? null : '/',
        currentUser: action.payload.error ? null : action.payload.success.user
      };
    case LOGIN:
    case REGISTER:
      return {
        ...state,
        redirectTo: action.payload.error ? null : '/',
        token: action.payload.error ? null : action.payload.success.token,
        currentUser: action.payload.error ? null : action.payload.success.user
      };
    case HOME_PAGE_UNLOADED:
    case PROFILE_PAGE_UNLOADED:
    case SETTINGS_PAGE_UNLOADED:
    case LOGIN_PAGE_UNLOADED:
    case REGISTER_PAGE_UNLOADED:
      return { ...state, viewChangeCounter: state.viewChangeCounter + 1 };
    default:
      return state;
  }
};
