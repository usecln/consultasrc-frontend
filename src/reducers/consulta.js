import { CONSULTA_PAGE_LOADED, CONSULTA_PAGE_UNLOADED } from '../constants/actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case CONSULTA_PAGE_LOADED:
      return {
        ...state
      };
    case CONSULTA_PAGE_UNLOADED:
      return {};
    default:
      return state;
  }
};
