import { USERS_PAGE_LOADED, USERS_PAGE_UNLOADED } from '../constants/actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case USERS_PAGE_LOADED:
      return {
        ...state,
        usuarios: action.payload.map(user => {
          user.acceso_movil = Number(user.acceso_movil) === 0? false: true;
          return user;
        })
      };
    case USERS_PAGE_UNLOADED:
      return {};
    default:
      return state;
  }
};