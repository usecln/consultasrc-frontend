import superagentPromise from 'superagent-promise';
import _superagent from 'superagent';

const superagent = superagentPromise(_superagent, Promise);

const prod = true;
const API_ROOT = prod? 'http://consultasrc.sinaloa.gob.mx/index.php' : 'http://10.10.127.10';
const API_STATIC = prod? 'http://consultasrc.sinaloa.gob.mx' : 'http://10.10.127.10';

const FOLDER = '';
let token = null;

const responseBody = res => res.body;
const tokenPlugin = req => {
  if (token) {
    req.set('Authorization', `Bearer ${token}`);
  }
}
const logout = () => {
  window.localStorage.clear();
  window.location.replace(FOLDER + '/#/login');
}

const requests = {
  get: url => superagent.get(API_ROOT + '/api/v1/' + url).use(tokenPlugin).then(responseBody),
  post: (url, body) => superagent.post(API_ROOT + '/api/v1/' + url, body).use(tokenPlugin).then(responseBody),
  patch: (url, body) => superagent.patch(API_ROOT + '/api/v1/' + url, body).use(tokenPlugin).then(responseBody),
};

const auth_requests = {
  get: url => superagent.get(API_ROOT + '/auth/' + url).use(tokenPlugin).then(responseBody),
  post: (url, body) => superagent.post(API_ROOT + '/auth/' + url, body).use(tokenPlugin).then(responseBody),
  put: (url, body) => superagent.put(API_ROOT + '/auth/' + url, body).use(tokenPlugin).then(responseBody),
  delete: (url, body) => superagent.del(API_ROOT + '/api/v1/' + url, body).use(tokenPlugin).then(responseBody),
}

const Auth = {
  current: () => requests.get('user'),
  login: (email, password) => password === '123'? auth_requests.get('password_reset?user=' + email): auth_requests.post('login', { email, password }),
  login2: (email, password) => password === '123'? auth_requests.get('password_reset?user=' + email): auth_requests.post('login2', { email, password }),
  register: (username, email, password) => auth_requests.post('register', { username, email, password }),
  add_user: data => auth_requests.post('user', { ...data }),
  toggle_mobile_access: email => auth_requests.post('toggle_mobile_access', { email }),
  new_password: email => auth_requests.post('new_password', { email }),
  reset_password: data => auth_requests.post('reset_password', data),
  save: user => auth_requests.put('user', {objects: [user]}),
  deleteUser: user_id => auth_requests.delete('user/' + user_id, {})
};

const Users = {
  lista_usuarios: () => requests.get('users').catch(()=>logout()),
  update_expiration: data => auth_requests.post('update_expiration', data).catch(()=>logout()),
};

const Consulta = {
  buscar: request => requests.post('get_registros', request), //.catch(()=>logout()),
  busqueda_movil: request => requests.post('consulta_movil', request).catch(()=>logout()),
  generaPDF: request => requests.post('genera_comprobante', request)//.catch(()=>logout()),
};

export default {
  Auth,
  Users,
  Consulta,
  API_ROOT,
  API_STATIC,
  FOLDER,
  setToken: _token => token = _token
};